## missi_pad_global-user 13 TP1A.220624.014 V14.0.1.0.TLYMIXM release-keys
- Manufacturer: xiaomi
- Platform: common
- Codename: mihal
- Brand: Xiaomi
- Flavor: missi_pad_global-user
- Release Version: 13
- Kernel Version: 5.10.101
- Id: TP1A.220624.014
- Incremental: V14.0.1.0.TLYMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Xiaomi/mihal/mihal:12/SP1A.210812.016/V14.0.1.0.TLYMIXM:user/release-keys
- OTA version: 
- Branch: missi_pad_global-user-13-TP1A.220624.014-V14.0.1.0.TLYMIXM-release-keys
- Repo: xiaomi_mihal_dump_10385
